# Desafio Front-End
Primeiramente, ficamos felizes com seu interesse em trabalhar com a gente! Abaixo você encontrará todos as informações necessárias para iniciar o teste. Leia com atenção o exercício proposto e caso tenha alguma dúvida pode falar com a gente. Boa sorte!

## Avisos antes de começar

- Para iniciar o teste, crie uma branch a partir da `main` nesse padrão de nomenclatura: dd-mm-yy/nome-sobrenome (por exemplo, 30-04-20/meu-nome)
- Quando finalizar o teste, cria uma `merge request` da sua branch para a main e avise o(a) recrutador(a) que o teste está finalizado
- Descreva no README as instruções exatas para executar o projeto

## Setup do projeto

- Vue 2
- Node: 14.16.1
- Axios
- JSON Server
- Bootstrap

### Leia com atenção

Em nosso desafio, você deverá criar uma tela de listagem de funcionários, exibindo nomes, e-mails e seus respectivos departamentos, onde também será possível adicionar, editar e excluir um funcionário. A ação de adicionar e editar funcionário deverá ser realizada em um modal que aparecerá no momento da ação. Também será necessário uma tela de listagem dos departamentos da empresa e as ações e condições são as mesmas já citadas anteriormente. Toda alteração efetuada, tenha ela sucesso ou não, deverá ser exibida em um modal e contar com um aviso de sucesso ou falha, visando uma boa experiência do usuário. Para estilizar sua aplicação, deve-se utilizar Bootstrap.

Temos uma API mock feita utilizando [JSON Server](https://github.com/typicode/json-server) na qual você utilizará para implementar os seus serviços de CRUD. Os dados estão armazenados no arquivo `db.json` na raiz do projeto.

**Certifique-se de revisar seu código, de modo que fique o mais organizado possível para a avaliação e entendimento dos avaliadores.**

Utilize a estrutura de pastas que achar mais adequada, bem como padrões, patterns, práticas de segurança, performance etc.

O diferencial para este desafio, é o aprimoramento do mesmo, bem como implementação de práticas de segurança, performance e/ou estrutura.

### **API**

Para utilizar a API mock é necessário que você instale-o globalmente em sua máquina para ter os recursos da lib.

**1 -** Como instalar?
`npm install -g json-server`

Link para mais detalhes: https://github.com/typicode/json-server

#### ROTAS

**Funcionários**

`GET: /funcionarios`

`POST: /funcionarios`

`PUT: /funcionarios`

`PATCH: /funcionarios`

`DELETE: /funcionarios`

**Departamentos**

`GET: /deptos`

`POST: /deptos`

`PUT: /deptos`

`PATCH: /deptos`

`DELETE: /deptos`



### Boa sorte!
